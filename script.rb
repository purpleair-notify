#!/usr/bin/env ruby --jit

=begin
Copyright 2019 Wu Ming 2 at wu.ming2@icloud.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
=end

require 'net/http'
require 'json'
require 'discordrb'
require 'hashdiff'
require 'redis'
require 'time'
require 'date'

class Config

	# Threshold
	MY_THRESHOLD = 10.0		# PM2.5 µg/m³

	# Discord
	DISCORD_CHANNEL_ID = <Channel ID>
	DISCORD_AUTHOR_ID = <Your ID>	# just one author for now
	DISCORD_TOKEN = '<Token>'
	DISCORD_HISTORY_SIZE = 10  # increase if more chatty

	PA_URI = URI('https://www.purpleair.com/json?show=<Your PA ID>')

	# Implement Observatory module after updating this
	OB_URI = URI('<Alternate source of T and Humidity source URL>')
	LOCATION = "<Alternate source of T and Humidity source display name>"

	# Timing
	TIME = Time.now
	# 30' heartbeat
	TIME_10_FLAG = TIME.min.between?(01,19)
	TIME_40_FLAG = TIME.min.between?(31,49)
	TIME_BEGIN_PREV = TIME-30*60
	# night is off
	NIGHTTIME_FLAG = TIME.hour.between?(00,05)
	# night period boundaries
	EXP_REF = NIGHTTIME_FLAG ? 5 : 0 
	DAILY_FLAG = TIME.hour == 8
	WEEKLY_FLAG = DAILY_FLAG && TIME.sunday?

	# Cache
	MESSAGING = {"!start" => false, "!stop" => true}

	# General exlusion period
	exit(0) if Config::TIME_10_FLAG || Config::TIME_40_FLAG
end

module Proceed
	# Keeps quiet if user wishes so or at night time
	MessagesNotAllowedNow = proc do |cached_wish|
		cached_wish ||= CacheControl::GetWish.call
		# true means !stop
		# user wish always prevails if not nil
		# dynamic flag before | after Discord update
		[ Config::MESSAGING[cached_wish], Config::NIGHTTIME_FLAG ].compact.first
	end
	IsAboveThreshold = proc {Purple::Current > Config::MY_THRESHOLD}
	WasBelowThreshold = proc {Purple::Hour_Avg <= Config::MY_THRESHOLD}
	BelowThresholdAgain =
		# Avoids messaging if notified already below threshold
		proc {!IsAboveThreshold.call && WasBelowThreshold.call}
	IsHumidityOutOfRange = proc {!Observatory::Humidity.to_i.between?(30,70)}
end

module NeedToKnow
	result = JSON.parse(File.read('purple_dump.json'))["results"][0] # Hash
	res_a_keys = result.keys	# Array
	stats_a_keys = JSON.parse(result["Stats"]).keys	# Array
	check_diff = proc do |keys1, keys2|
		Hashdiff.diff(keys1,keys2)
	end

	# Just to try currying
	Check_res = check_diff.curry.call(res_a_keys)
	Check_stats = check_diff.curry.call(stats_a_keys)
end

module Conn
	#Bot = Discordrb::Bot.new(token: Config::DISCORD_TOKEN, ignore_bots: true)
	#Channel = Bot.channel(Config::DISCORD_CHANNEL_ID)
	PurpleData = Net::HTTP.get(Config::PA_URI)
	Red = Redis.new
end

module BotControl
	# Discordrb::Bot is not intended to be used without bot.run
	#Start = proc {Conn::Bot.run(:async)} # returns Proc instead of executing at assignment 
	#Stop = proc {Conn::Bot.stop; exit(0)}  # returns an ERROR message
	#Send = proc {|msg| Conn::Bot.send_message(Conn::Channel, msg)}
	#Quit = proc {|msg| (Stop<<Send).call(msg)}
	response = Discordrb::API::Channel.messages(	# gets last messages
		"Bot "+Config::DISCORD_TOKEN, Config::DISCORD_CHANNEL_ID, 
		Config::DISCORD_HISTORY_SIZE )	# newest first
	parse = proc { |resp| JSON.parse(resp) } # RestClient::Response -> [(0..n)Hash]
	by_time = proc { |a|	#  [(0..n)Hash] -> [(0..n)Hash]
		a.select { |m| 
			Time.parse(m["timestamp"]).getlocal >= Config::TIME_BEGIN_PREV } }
	by_author = proc { |a|	#  [(0..n)Hash] -> [(0..n)Hash] 
		a.select{ |m|
			m["author"]["id"] == Config::DISCORD_AUTHOR_ID.to_s } }
	by_content = proc { |a|		# [(0..n)Hash] -> nil or Str
		Config::MESSAGING.keys.find { |k| 
			a.any? { |m| m["content"].start_with?(k) } } }

	# public
	Send = proc {|msg| Discordrb::API::Channel.create_message(
		"Bot "+Config::DISCORD_TOKEN, Config::DISCORD_CHANNEL_ID, msg )}
	Wish = (by_content<<by_author<<by_time<<parse).call(response)
end

module CacheControl
	getFlag = proc {|flag| Conn::Red.get(flag)} # -> nil or Str
	
	expSec = (
		t = Time.now
		# from midnight of tomorrow or
		# from REF of today
		d = Config::EXP_REF == 0 ? Date.today + 1 : Date.today
		# return expiration time in sec from REF
		( Time.new(d.year,d.month,d.day,Config::EXP_REF,0,0) - t ).to_i
	)
	# User wish for start/stop
	GetWish = proc {getFlag.call("messaging")} # changes w update
	setWish = proc do |wish,cached_wish|
		next unless wish
		# valid user wish is always opposite of time-based flag
		# if cache doesn't exist and is valid create cache
		# if cache exist is opposite already so delete
		cached_wish.nil? ? 
			Conn::Red.set("messaging", wish, :ex => expSec) :
			Conn::Red.del("messaging")
	end
	userWish = proc do |user_wish|
		# see .gv file for logic
		# More clever solution for binary decision tree surely exist.
		next unless user_wish	# no news from user, returns nil
		cached_wish = GetWish.call
		# cached flag or time-based flag
		sys_wish = Proceed::MessagesNotAllowedNow.call(cached_wish)
		next if sys_wish == Config::MESSAGING[user_wish] # if old or silly news
		BotControl::Send.call("#{user_wish} got it! Until next period change.")
		[user_wish,cached_wish]
		# returns nil or [user,nil] or [user,cached]
	end
	autoUpdate = (setWish<<userWish).call(BotControl::Wish)
end

module Purple
	results = JSON.parse(Conn::PurpleData)["results"] # [(2)]
	stats_a = JSON.parse(results[0]["Stats"])	# Hash
	stats_b = JSON.parse(results[1]["Stats"])	# Hash
	value = proc {|key| (stats_a[key].to_f + stats_b[key].to_f) / 2}
	
	# check if data format changed
	Res_diff = NeedToKnow::Check_res.call(results[0].keys)
	Stats_diff = NeedToKnow::Check_stats.call(stats_a.keys)
	
	# public
	Age = results[0]["AGE"]
	Current = value.call("v1")
	Hour_Avg = value.call("v3")
	Day_Avg = value.call("v5")
	Week_Avg = value.call("v6")
	#Failure = A_G not available anymore??
end

module Observatory
	# low resilience to page formatting changes
	Temperature, Humidity = Net::HTTP.get(Config::OB_URI).lines.<implement>
end

module Messages
	msg = Proceed::IsAboveThreshold.call ? "START" : "OK"
	humidity = Proceed::IsHumidityOutOfRange.call ? "Humidity needs active regulation!!" : ""
	particulate = "Average: last ten min #{Purple::Current.to_i.to_s}."
	temperature = "Temperature #{Observatory::Temperature.to_s}°C in #{Config::LOCATION}."
	daily = Config::DAILY_FLAG ? "Average: last day #{Purple::Day_Avg.to_i.to_s}." : ""
	weekly = Config::WEEKLY_FLAG ? "Average: last week #{Purple::Week_Avg.to_i.to_s}." : ""
	res_diff = Purple::Res_diff.empty? ? "" : "Channel changed: #{Purple::Res_diff}"
	stats_diff = Purple::Stats_diff.empty? ? "" : "Stats changed: #{Purple::Stats_diff}"
	
	# public
	Message = (
		msg + " " + particulate + " " + humidity + " " +
		temperature + " " + daily + " " + weekly + " " +
		res_diff + " " + stats_diff
	)
	Age = Purple::Age > 10 ? "Data #{Purple::Age} minutes old." : "" 
	#Failure = "Possible hardware issue: #{@readings[:failure]}" if Purple::Failure??
end
