#!/usr/bin/env ruby --jit

=begin
Copyright 2019 Wu Ming 2 at wu.ming2@icloud.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
=end

require_relative 'script'

module MainControl
	exit(0) if Proceed::MessagesNotAllowedNow.call
	# send Age warning anyway
	BotControl::Send.call(Messages::Age) unless Messages::Age.empty?
	# if notified OK already do nothing
	exit(0) if Proceed::BelowThresholdAgain.call
	# I could compose the four functions
	BotControl::Send.call(Messages::Message)
end
